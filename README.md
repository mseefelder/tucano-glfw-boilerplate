# [GLFW](http://www.glfw.org/) + [Tucano](https://gitlab.com/lcg/tucano) project boilerplate

Boilerplate for OpenGL application using GLFW for window management and the Tucano library.

Tested and working on Ubuntu and Windows.

## Requirements

**(requirement setup instructions coming soon)**

You should clone the Tucano repository. No need to compile.

You should have GLFW.

## To compile & run:

```
mkdir build
cd build
cmake -DTUCANO_ROOT:PATH="your path to the root dir of Tucano" ..
make
```

This will create a `bin` directory on your project root. To execute the project, do:

```
cd bin
./app
```

For now, **you must** `cd` **into the** `bin` **directory** to run the application because the shader path is relative.