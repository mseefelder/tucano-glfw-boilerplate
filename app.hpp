#ifndef __APP__
#define __APP__

#include <GL/glew.h>

#include <renderer.hpp>
#include <shapes/camerarep.hpp>
#include <mesh.hpp>
#include <utils/objimporter.hpp>
#include <utils/plyimporter.hpp>
#include <utils/trackball.hpp>
#include "trackballcamera.hpp"

using namespace std;

class App 
{

public:

    explicit App(void);
    ~App();
    
    /**
     * @brief Initializes the shader effect
	 * @param width Window width in pixels
	 * @param height Window height in pixels
     */
    void initialize(int width, int height);

    /**
     * Repaints screen buffer.
     **/
    virtual void paintGL();

	/**
	* Returns the pointer to the flycamera instance
	* @return pointer to flycamera
	**/
	//Flycamera* getCamera(void)
	Tucano::Trackballcamera* getCamera(void)
	{
		return &flycamera;
	}

	Tucano::Effects::Renderer* getEffect(void)
	{
		return &renderer;
	}

	void changeMesh(void);

private:

	// A simple phong shader for rendering meshes
    Tucano::Effects::Renderer renderer;

	// A fly through camera
	Tucano::Trackballcamera flycamera;

	// Light represented as a camera
	Tucano::Camera light;
	
	// Meshes
	Tucano::Mesh tri;
};

#endif // APP
