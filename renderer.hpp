#ifndef __RENDERER__
#define __RENDERER__

#include <tucano.hpp>
#include <camera.hpp>

namespace Tucano
{
namespace Effects
{

/**
 * @brief Renders a mesh using a Renderer shader.
 */
class Renderer : public Effect
{

private:

    /// Renderer Shader
    Shader renderer_shader;

    /// Default color
    Eigen::Vector4f default_color;

    /// Number of vertices per patch, inner subdiv, outer subdiv
    int patchVertices;
    float inner, outer;

    /// Shader swap state
    bool swapState;

public:

    /**
     * @brief Default constructor.
     */
    Renderer (void)
    {
        default_color << 0.7, 0.4, 0.1, 1.0;
        patchVertices = 3;
        inner = 1.0;
        outer = 1.0;
        swapState = false;
    }

    /**
     * @brief Default destructor
     */
    virtual ~Renderer (void) {}

    /**
     * @brief Load and initialize shaders
     */
    virtual void initialize (void)
    {
        // searches in default shader directory (/shaders) for shader files rendererShader.(vert,frag,geom,comp,tesc,tese)
        loadShader(renderer_shader, "render");
    }

    bool swapShader (void)
    {
        return true;
    }

    /**
    * @brief Sets the default color, usually used for meshes without color attribute
    */
    void setDefaultColor ( Eigen::Vector4f& color )
    {
        default_color = color;
    }

    /** * @brief Render the mesh given a camera and light, using a Renderer shader 
     * @param mesh Given mesh
     * @param camera Given camera 
     * @param lightTrackball Given light camera 
     */
    void render (Tucano::Mesh& mesh, const Tucano::Camera& camera, const Tucano::Camera& lightTrackball)
    {

        Eigen::Vector4f viewport = camera.getViewport();
        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

        renderer_shader.bind();

        // sets all uniform variables for the renderer shader
        renderer_shader.setUniform("projectionMatrix", camera.getProjectionMatrix());
        renderer_shader.setUniform("modelMatrix", mesh.getModelMatrix());
        renderer_shader.setUniform("viewMatrix", camera.getViewMatrix());
        renderer_shader.setUniform("lightViewMatrix", lightTrackball.getViewMatrix());
        renderer_shader.setUniform("default_color", default_color);
        renderer_shader.setUniform("ka",0.3f);
        renderer_shader.setUniform("kd",0.5f);
        renderer_shader.setUniform("ks",0.2f);

        mesh.setAttributeLocation(renderer_shader);

        glEnable(GL_DEPTH_TEST);

        mesh.render();

        renderer_shader.unbind();
    }


};

}
}

#endif
