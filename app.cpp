#include "app.hpp"

App::App(void)
{
}

App::~App()
{}


void App::initialize (int width, int height)
{
    // initialize the shader effect
    renderer.setShadersDir("../shaders/");
    renderer.initialize();

    flycamera.setPerspectiveMatrix(60.0, width/(float)height, 0.1f, 100.0f);
    // left side of the window (window x dim = 2*width+20)
    flycamera.setViewport(Eigen::Vector2f ((float)width, (float)height));

	Tucano::MeshImporter::loadPlyFile(&tri, "../models/model.ply");
	tri.normalizeModelMatrix();
	
}

void App::paintGL (void)
{
   	flycamera.updateViewMatrix();
   	Eigen::Vector4f viewport = flycamera.getViewport(); 
	
 	glClearColor(0.9, 0.9, 0.9, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	renderer.render(tri, flycamera, light);
}
